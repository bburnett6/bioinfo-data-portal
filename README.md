# Data Portal for Bioinformatics

## Project Overview

- Nginx Proxy: Exposes the public services and hides infrastructure services.
- Flask Portal: User interaction service. Users intially register their accounts here to recieve API access.
- Kinto API: API service for data sets. The API requires an API token for use, granted upon registration. 
- PostgreSQL: A private database used by Kinto. In theory, this could be used in the portal as well to consolodate everything, but the flask plugins for sqlite are very convenient.

## Cloning

This repository uses submodules for some libraries used. To clone use

`git clone --recurse-submodules`

## Deploying

To run in the backround (useful for deployment):

`docker-compose up -d` 

When running it as a daemon, check on everything with 

`docker ps`

and take it down with

`docker-compose down`

To run interactively (useful for debugging because live reload of the flask server should work when files are updated):

`docker-compose up --build` 

*Note* There have been instances when deploying for the first time that the `pdb` service does not come up correctly. If this happens, take down everything (ctrl-c if using `--build` or `docker-compose down` if using `-d`) and simply restart.

## Database notes

The database needs somewhere to store all persistent data. Change the volumes under the pdb service to point somewhere to `/var/lib/postgresql/data` in the docker-compose for this. 

## Nginx notes

Acts as a proxy for all of the services. Currently it only exposes the portal and api services. Might be worthwhile to set up rules to restrict access to certain endpoints of the API. 

## Portal notes

The portal currently uses SQLite3 to manage user registrations. Upon registration, a clone account is created with the API that uses a token to authenticate with. This is an in memory database but stores for subsequent use at the base of the portal directory in `app.db`. 

The portal requires a volume used for a download cache. This download cache's location on the host can be changed in the docker-compose under the portal service's volumes and should point to `/download-cache` within the container. If this location in the container is changed, be sure to change the `DOWNLOAD_CACHE` location in the app configuration file `config.py`. 

## API notes

Kinto is a very handy API service. It has built in access control list (ACL) support for restricting access to certain collections/records. There is also functionality to support a history of records. Refer to the [full API reference](https://docs.kinto-storage.org/en/stable/api/1.x/index.html#full-reference) for more details.

Kinto is organized with three types of containers: buckets, collections, and records. Their relationship is buckets contain collections, and collections contain records. The endpoint structure for Kinto's API then looks something like 'http://hostname:port/v1/buckets/_bucketname_/collections/_collectionname_/records/_recordname_'. You can then get all of the records in a collection by accessing 'http://hostname:port/v1/buckets/_bucketname_/collections/_collectionname_/records', get the metadata on a collection from 'http://hostname:port/v1/buckets/_bucketname_/collections/_collectionname_', get all collections from 'http://hostname:port/v1/buckets/_bucketname_/collections/_collectionname_' and so on. See the [concepts page](https://docs.kinto-storage.org/en/stable/concepts.html) on the Kinto wiki for more.

With this high-level understanding of Kinto, we can now go over the structure of how it is being used to store datasets. There is one bucket named datasets that is used to store each dataset as a collection. The individual files within the dataset are each stored in their own record. This structure was chosen so that it would be possible to use Kinto's built-in filtering to search through and collect individual files of a dataset as was requested. [More information on filtering/searching.](https://docs.kinto-storage.org/en/stable/api/1.x/filtering.html)

This structure also enables a permissions heirarchy that allows for protected datasets. Permissions in Kinto are inheritable, so a record inherits a collection's permissions which then inherits the bucket's permissions. The bucket datasets permissions are currently not set so that when a user creates a dataset (collection), the collection then has control over all permissions. Permissions can be assigned to individual accounts or groups and are split into read/write, with write implying read. The [tutorial on permissions](https://docs.kinto-storage.org/en/stable/tutorials/permission-setups.html) is the most useful for understanding this better.