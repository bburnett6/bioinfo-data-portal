
import os 

basedir = os.path.abspath(os.path.dirname(__file__)) #basedir of application

class Config(object):
	"""
	download-cache:
	The location on the system for storing files for download. Ideally this would
	be some kind of directory with a cron job that cleans out files that haven't
	been used for some time. 
	"""
	DOWNLOAD_CACHE = '/download-cache'
	"""
	admin_auth: 
	Authorization tuple used for contacting the Kinto API as admin.
	Structure: ('username', 'password')
	used in a request like: requests.get('http://api:9093/v1/accounts/admin', auth=ADMIN_AUTH)
	"""
	ADMIN_AUTH = ('admin', os.environ.get("KINTO_ADMIN_PASS", "passwd"))
	"""
	kinto_addr:
	Address of the kinto api server. Should be defined in docker-compose.yml
	Needs 'http://' or 'https://' depending on settings in kinto.ini
	'http' should be fine because we are working behind a proxy
	"""
	KINTO_ADDR = 'http://api:9093'
	"""
	secret_key:
	Used by flask-login to handle security. See their docs for more info
	"""
	SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", 'a-default-key-that-should-be-changed')
	"""
	sqlalchemy_database:
	Address of the sqlite instance used for portal user administration. Default
	is to just use an instance built in with the portal
	"""
	SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URL', f"sqlite:///{os.path.join(basedir, 'app.db')}")
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	"""
	Google OAuth Config 
	See: https://developers.google.com/identity/gsi/web/guides/get-google-api-clientid
	
	google_oauth_client_id:
	OAuth client id generated from the google API.

	google_oauth_client_secret:
	OAuth secret generated from the google API.
	"""
	GOOGLE_OAUTH_CLIENT_ID = '84090780671-4sh8jdoigf0qgjd583nq9mb83v0ovqot.apps.googleusercontent.com'
	GOOGLE_OAUTH_CLIENT_SECRET = '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'