
from flask import render_template, flash, redirect, url_for, request, send_from_directory
from flask_login import current_user, login_user, logout_user, login_required
from werkzeug.urls import url_parse
from app import app, db
from app.forms import LoginForm, RegistrationForm, NewPassForm, NewGroupForm, NewDatasetForm, DatasetSearchForm, DatasetFilterDownloadForm, DatasetSelectDownloadForm
from app.models import User
from pathlib import Path 
import requests
import tempfile
import shutil
import base64
import json
import sys
import os 

@app.route('/index')
def index():
	return redirect(url_for('browse'))

@app.route('/')
@app.route('/browse')
def browse():
	browse_limit = 4
	"""
	if the flask request headers has a kinto limit token, use that in the requests
	address. If not, then start a browse request with the default browse address
	"""
	token = request.args.get('_token', default=None, type=str)
	if token is not None:
		#print(f"browse token: {token}", file=sys.stderr)
		browse_addr = f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections?_limit={browse_limit}&_token={token}"
		#print(f"Next page browse address: {browse_addr}", file=sys.stderr)
	else:
		#print(f"No token", file=sys.stderr)
		browse_addr = f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections?_limit={browse_limit}"
	
	#Check user authority for request		
	if current_user.is_anonymous: 
		#Search with no authority and show public datasets only
		user_auth = None
	else: 
		#Search with the current users authority so that allowed datasets are shown
		user_auth = (current_user.username, current_user.get_api_token())
	
	r = requests.get(browse_addr, auth=user_auth)
	if r.status_code != 200:
		print(f"Error browsing! This should always work (unless there are no datasets) so API is likely broken...", file=sys.stderr)
		print(f"Status: {r.status_code}", file=sys.stderr)

	"""
	if there are more datasets, the kinto response headers will
	have a token that can be used to get the next set of 
	collections
	"""
	if "Next-Page" in r.headers:
		"""
		get the next page token. 
		"""
		print(f"Next page headers: {r.headers['Next-Page']}", file=sys.stderr)
		next_page = r.headers['Next-Page'].split('?') 
		next_page_token = next_page[-1].split('=')[-1]
		next_page_headers = {'_token', next_page_token}
	else:
		next_page_token = None
		next_page_headers = None

	"""
	request json results have shape:
	{
		"data": [list of collections (empty if no results)]
	}
	so use that list of collections.
	Collections then have shape of a dataset collection
	{'id': ..., 'tags': ..., 'contents': ..., 'author': ...}

	next_page_headers allows for further browsing from this 
	route. Attach it to a 'next page' button using 
	url_for('browse', headers=next_page_headers)
	"""
	res = None
	if 'data' in r.json():
		res = r.json()['data']
	return render_template('browse.html', results=res, next_page_token=next_page_token)

###############
# User Login
###############

"""
@app.route('/login', methods=['GET', 'POST'])
def login():
	if current_user.is_authenticated:
		return redirect(url_for('index'))

	form = LoginForm()

	if form.validate_on_submit():
		#verify user
		user = User.query.filter_by(username=form.username.data).first()
		if user is None or not user.check_password(form.password.data):
			flash('Invalid username or password. Try again.')
			return redirect(url_for('login'))
		#login user
		login_user(user, remember=form.remember_me.data)
		next_page = request.args.get('next')
		if not next_page or url_parse(next_page).netloc != '':
			next_page = url_for('browse')
		flash(f'Welcome {form.username.data}!')
		return redirect(next_page)

	return render_template('login.html', form=form)
"""

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return redirect(url_for('index'))

"""
@app.route('/register', methods=['GET', 'POST'])
def register():
	if current_user.is_authenticated:
		return redirect(url_for('index'))

	form = RegistrationForm()

	#create account. Validation makes sure this put is okay
	if form.validate_on_submit():
		#initialize user in database
		user = User(username=form.username.data, email=form.email.data)
		user.set_password(form.password.data)
		user.gen_api_token()
		db.session.add(user)
		db.session.commit()
		#create users api account 
		data = {"data": {"password": user.get_api_token()}}
		r = requests.put(f"{app.config['KINTO_ADDR']}/v1/accounts/{form.username.data}", json=data)
		if r.status_code != 201:
			print(f"Error initializing user, {form.username.data}, with api. Data: {data}. Status: {r.status_code}.", file=sys.stderr)
		flash('Thank you for registering! Please sign in to continue')
		return redirect(url_for('login'))

	return render_template('register.html', form=form)

"""

@app.route('/register')
def register():
	if current_user.is_authenticated:
		return redirect(url_for('index'))
	else:
		return redirect(url_for('google.login'))

############
# Settings
############

@app.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():

	# Reset password
	passform = NewPassForm()

	if passform.validate_on_submit():
		current_user.set_password(passform.password.data)
		db.session.commit() 
		flash('Password reset successful.')
		return redirect(url_for('settings'))

	# User permissions
	print((current_user.username, current_user.get_api_token()), file=sys.stderr)
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1", auth=(current_user.username, current_user.get_api_token()))
	print(r.json(), file=sys.stderr)
	user_api_info = r.json()['user']
	group_permissions = []
	system_permissions = []
	account_permissions = []
	for perm in user_api_info['principals']:
		if "system." in perm:
			system_permissions.append(perm.split('.')[-1])
		elif "groups/" in perm:
			group_permissions.append(perm.split('/')[-1])
		elif "account:" in perm:
			account_permissions.append(perm.split(':')[-1])
		else:
			print(f"Unknown permission, {perm}, for user, {current_user.username}", file=sys.stderr)

	return render_template(
		'settings.html', 
		passform=passform, 
		a_perms=account_permissions, 
		g_perms=group_permissions, 
		s_perms=system_permissions
		)

@app.route('/settings/create_group', methods=['GET', 'POST'])
@login_required
def create_group():
	form = NewGroupForm()

	if form.validate_on_submit():
		members = [f'account:{current_user.username}'] #add current user too
		admins = [f'account:{current_user.username}'] 

		if form.memberlist.data: #make sure not empty string
			for user in form.memberlist.data.split(','):
				members.append(f'account:{user}')
		if form.grouplist.data:
			for group in form.grouplist.data.split(','):
				members.append(f'group:{group}')
		if form.adminlist.data:
			for admin in form.adminlist.data.split(','):
				admins.append(f'account:{admin}')

		rjson = {'data': {'id': form.groupname.data, 'members': members}, 'permissions': {'write': admins}}
		r = requests.post(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/groups", json=rjson, auth=app.config['ADMIN_AUTH'])
		if r.status_code != 201:
			print(f"Error creating group {form.groupname.data}. Status: {r.status_code}", file=sys.stderr)
			print(f"data: {rjson}", file=sys.stderr)

		flash(f'Created group {form.groupname.data}')
		return redirect(url_for('settings'))

	return render_template('create_group.html', form=form)

############
# Datasets
############

@app.route('/create_dataset', methods=['GET', 'POST'])
@login_required
def create_dataset():
	form = NewDatasetForm()

	if form.validate_on_submit():
		#Create collection. One collection represents one dataset
		collection_name = "_".join(form.title.data.lower().split(' '))
		collection_files = []
		for file in form.files.data:
			collection_files.append(file.filename)
		collection_data = {'data': {'id': collection_name, 'tags': form.tags.data, 'contents': collection_files, 'author': current_user.fullname}}

		writers = [f'account:{current_user.username}']
		if form.world_readable.data:
			readers = ['system.Everyone']
		else:
			readers = [f'account:{current_user.username}']

		if form.account_read_permissions.data:
			for user in form.account_read_permissions.data.split(','):
				readers.append(f'account:{user}')
		if form.account_write_permissions.data:
			for user in form.account_write_permissions.data.split(','):
				writers.append(f'account:{user}')
		if form.group_read_permissions.data:
			for group in form.group_read_permissions.data.split(','):
				readers.append(f'/buckets/datasets/groups/{group}')
		if form.group_write_permissions.data:
			for group in form.group_write_permissions.data.split(','):
				readers.append(f'/buckets/datasets/groups/{group}')
		collection_data['permissions'] = {'write': writers, 'read': readers}

		r = requests.post(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections", json=collection_data, auth=app.config['ADMIN_AUTH'])
		if r.status_code != 201:
			print(f"Error creating dataset collection {collection_name}. Status: {r.status_code}", file=sys.stderr)
			print(f"data: {collection_data}", file=sys.stderr)

		#Create records. One record for each file. Inherits permissions from parent collection
		for file in form.files.data:
			#File is a werkzeug FileStorage type. See https://stackoverflow.com/questions/20015550/read-file-data-without-saving-it-in-flask
			record_id = Path(file.filename).stem 
			fname = file.filename
			ftype = file.content_type
			fencoded = base64.b64encode(file.stream.read()).decode('utf-8') #encode as base64 bytes then decode as base64 string

			record_data = {'data': {'id': record_id, 'filename': fname, 'type': ftype, 'encoding': 'base64', 'file': fencoded}}
			r = requests.post(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{collection_name}/records", json=record_data, auth=app.config['ADMIN_AUTH'])
			if r.status_code != 201:
				print(f"Error creating dataset record {fname}. Status: {r.status_code}", file=sys.stderr)
				print(f"data: {record_data}", file=sys.stderr)

		flash(f'Created dataset {form.title.data}')
		return redirect(url_for('index'))

	return render_template('create_dataset.html', form=form)

@app.route('/search_datasets', methods=['GET', 'POST'])
def search_datasets():
	form = DatasetSearchForm()

	if form.validate_on_submit():
		#search string needs form ["tag1","tag2","tagn"]
		search_string = ",".join(map(lambda s: f'"{s}"', form.tags.data))
		search_string = f"[{search_string}]"
		
		if current_user.is_anonymous: 
			#Search with no authority and show public datasets only
			user_auth = None
		else: 
			#Search with the current users authority so that allowed datasets are shown
			user_auth = (current_user.username, current_user.get_api_token())
		
		r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections?contains_any_tags={search_string}", auth=user_auth)
		if r.status_code != 200:
			print(f"Error searching! This should always work so API is likely broken...", file=sys.stderr)
			print(f"Status: {r.status_code}", file=sys.stderr)
			print(f"Search string: {search_string}", file=sys.stderr)

		"""
		search results have shape:
		{
			"data": [list of collections (empty if no results)]
		}
		so use that list of collections.
		Collections then have shape of a dataset collection
		{'id': ..., 'tags': ..., 'contents': ..., 'author': ...}
		"""
		res = None
		if 'data' in r.json():
			res = r.json()['data']
		return render_template('search_results.html', results=res)

	return render_template('search_datasets.html', form=form)

@app.route('/datasets/<dataset_id>', methods=['GET', 'POST'])
def view_dataset(dataset_id):
	if current_user.is_anonymous: 
		#Search with no authority and show public datasets only
		user_auth = None
	else: 
		#Search with the current users authority so that allowed datasets are shown
		user_auth = (current_user.username, current_user.get_api_token())
	
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{dataset_id}", auth=user_auth)
	if r.status_code != 200:
		print(f"Error getting dataset {dataset_id} metadata", file=sys.stderr)
		print(f"Status: {r.status_code}", file=sys.stderr)
		return f"Error getting dataset {dataset_id}"

	#get choices to add to selection field
	select_form = DatasetSelectDownloadForm()
	select_form.dataset_list.choices = r.json()['data']['contents']
	if select_form.submitSelect.data and select_form.validate_on_submit(): #the and makes sure this form was submitted from and not the others
		return redirect(url_for('select_download_dataset', dataset_id=dataset_id, selections=select_form.dataset_list.data))

	filter_form = DatasetFilterDownloadForm()
	if filter_form.submitFilter.data and filter_form.validate_on_submit():
		return redirect(url_for('filter_download_dataset', dataset_id=dataset_id, id_filter=filter_form.id_filter.data))

	"""
	data is just a collection with shape 
	{'id': ..., 'tags': ..., 'contents': ..., 'author': ...}
	"""
	return render_template('view_dataset.html', result=r.json()['data'], filter_form=filter_form, select_form=select_form)

"""
download_dataset
Download the full dataset. First checks a cache to see if the download
is already prepared for download. If not in the cache, the file is prepared
and then sent along.
"""
@app.route('/datasets/<dataset_id>/full-download')
def download_dataset(dataset_id):
	#Get the dataset using users authority. The request will make sure the user can access the dataset. Need to error handle the request if not authorized
	if current_user.is_anonymous: 
		user_auth = None
	else: 
		user_auth = (current_user.username, current_user.get_api_token())

	#First check the download cache to see if the archive already exists. 
	#Don't get full dataset yet, just check if authorized
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{dataset_id}", auth=user_auth)
	if r.status_code != 200:
		#TODO: Handle the download error/unauthorized access better
		print(f'Error getting dataset metadata for download. Status: {r.status_code}', file=sys.stderr)
		#Just send the error for now.
		return r.json()
	#Now check if file exists, if True return dataset
	if os.path.isfile(os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}.zip')):
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}.zip')

	#If here, dataset is not in cache, so actually fetch and prepare
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{dataset_id}/records", auth=user_auth)
	if r.status_code != 200:
		#TODO: Handle the download error/unauthorized access better
		print(f'Error getting dataset for download. Status: {r.status_code}', file=sys.stderr)
		#Just send the error for now.
		return r.json() 

	records = r.json()['data']

	"""
	Prepare the dataset for download.
	Algorithm is to create a tmpdir on the server, write the files
	to that tmpdir, zip that tmpdir and store in the download cache,
	then send the zip
	"""
	try:
		with tempfile.TemporaryDirectory() as tmpdirname:
			dataset_zip_dirname = os.path.join(tmpdirname, dataset_id)
			os.mkdir(dataset_zip_dirname)

			for record in records:
				with open(os.path.join(dataset_zip_dirname, record['filename']), 'wb+') as f:
					f.write(base64.b64decode(record['file']))

			archive_name = os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}')
			shutil.make_archive(archive_name, 'zip', dataset_zip_dirname)
	
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}.zip')
	except Exception as e:
		print(f"Error creating download archive.", file=sys.stderr)
		print(e, file=sys.stderr)
		return "Error creating download"

"""
filter_download_dataset:
Download a slice of the dataset based on a filter provided by the user. 
Currently the only filter used is a search string filter that returns
a file if part of the title contains the filter. Download algorithm is
the same as download_dataset, and only the requests.get has been changed.
Example: 
	dataset:
		file_xxy.txt
		file_xyz.txt
		file2_xxy.txt
		file3_xxy.txt
		file_zzy.txt
		file_zzz.txt
	filter "xxy" returns:
		file_xxy.txt
		file2_xxy.txt
		file3_xxy.txt
	filter "zz" returns:
		file_zzy.txt
		file_zzz.txt

"""
@app.route('/datasets/<dataset_id>/filtered-download')
def filter_download_dataset(dataset_id):
	#Check that the query string exists
	id_filter_string = request.args.get('id_filter')
	if not id_filter_string:
		flash("Filter is required")
		return redirect(url_for('view_dataset', dataset_id=dataset_id))

	#Get the dataset using users authority. The request will make sure the user can access the dataset. Need to error handle the request if not authorized
	if current_user.is_anonymous: 
		user_auth = None
	else: 
		user_auth = (current_user.username, current_user.get_api_token())

	#First check the download cache to see if the archive already exists. 
	#Don't get full dataset yet, just check if authorized
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{dataset_id}", auth=user_auth)
	if r.status_code != 200:
		#TODO: Handle the download error/unauthorized access better
		print(f'Error getting dataset metadata for download. Status: {r.status_code}', file=sys.stderr)
		#Just send the error for now.
		return r.json()
	#Now check if file exists, if True return dataset
	if os.path.isfile(os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_filter_string}.zip')):
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_filter_string}.zip')

	#If here, dataset is not in cache, so actually fetch and prepare
	r = requests.get(f'{app.config["KINTO_ADDR"]}/v1/buckets/datasets/collections/{dataset_id}/records?like_id="{id_filter_string}"', auth=user_auth)
	if r.status_code != 200:
		#TODO: Handle the download error/unauthorized access better
		print(f'Error getting dataset for download. Status: {r.status_code}', file=sys.stderr)
		#Just send the error for now.
		return r.json() 

	records = r.json()['data']

	"""
	Prepare the dataset for download.
	Algorithm is to create a tmpdir on the server, write the files
	to that tmpdir, zip that tmpdir and store in the download cache,
	then send the zip
	"""
	try:
		with tempfile.TemporaryDirectory() as tmpdirname:
			dataset_zip_dirname = os.path.join(tmpdirname, f'{dataset_id}-{id_filter_string}')
			os.mkdir(dataset_zip_dirname)

			for record in records:
				with open(os.path.join(dataset_zip_dirname, record['filename']), 'wb+') as f:
					f.write(base64.b64decode(record['file']))

			archive_name = os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_filter_string}')
			shutil.make_archive(archive_name, 'zip', dataset_zip_dirname)
	
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_filter_string}.zip')
	except Exception as e:
		print(f"Error creating download archive.", file=sys.stderr)
		print(e, file=sys.stderr)
		return "Error creating download"

@app.route('/datasets/<dataset_id>/selected-download')
def select_download_dataset(dataset_id):
	#Check that the query string exists
	selections = request.args.getlist('selections')
	if not selections:
		flash("At least one is required")
		return redirect(url_for('view_dataset', dataset_id=dataset_id))

	#print(f'selections: {selections}', file=sys.stderr)
	base_names = list(map(lambda s: Path(s).stem, selections))
	id_select_string = '-'.join(base_names)

	#Get the dataset using users authority. The request will make sure the user can access the dataset. Need to error handle the request if not authorized
	if current_user.is_anonymous: 
		user_auth = None
	else: 
		user_auth = (current_user.username, current_user.get_api_token())

	#First check the download cache to see if the archive already exists. 
	#Don't get full dataset yet, just check if authorized
	r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/collections/{dataset_id}", auth=user_auth)
	if r.status_code != 200:
		#TODO: Handle the download error/unauthorized access better
		print(f'Error getting dataset metadata for download. Status: {r.status_code}', file=sys.stderr)
		#Just send the error for now.
		return r.json()
	#Now check if file exists, if True return dataset
	if os.path.isfile(os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_select_string}.zip')):
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_select_string}.zip')

	#If here, dataset is not in cache, so actually fetch and prepare
	records = []
	for base_name in base_names:
		r = requests.get(f'{app.config["KINTO_ADDR"]}/v1/buckets/datasets/collections/{dataset_id}/records/{base_name}', auth=user_auth)
		if r.status_code != 200:
			#TODO: Handle the download error/unauthorized access better
			print(f'Error getting dataset for download. Status: {r.status_code}', file=sys.stderr)
			#Just send the error for now.
			return r.json() 
		records.append(r.json()['data'])

	"""
	Prepare the dataset for download.
	Algorithm is to create a tmpdir on the server, write the files
	to that tmpdir, zip that tmpdir and store in the download cache,
	then send the zip
	"""
	try:
		with tempfile.TemporaryDirectory() as tmpdirname:
			dataset_zip_dirname = os.path.join(tmpdirname, f'{dataset_id}-{id_select_string}')
			os.mkdir(dataset_zip_dirname)

			for record in records:
				with open(os.path.join(dataset_zip_dirname, record['filename']), 'wb+') as f:
					f.write(base64.b64decode(record['file']))

			archive_name = os.path.join(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_select_string}')
			shutil.make_archive(archive_name, 'zip', dataset_zip_dirname)
	
		return send_from_directory(app.config['DOWNLOAD_CACHE'], f'{dataset_id}-{id_select_string}.zip')
	except Exception as e:
		print(f"Error creating download archive.", file=sys.stderr)
		print(e, file=sys.stderr)
		return "Error creating download"