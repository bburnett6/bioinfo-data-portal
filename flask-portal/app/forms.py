from flask_wtf import FlaskForm
from wtforms import Field, StringField, PasswordField, BooleanField, SubmitField, SelectField, MultipleFileField, widgets, SelectMultipleField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from wtforms.widgets import TextInput
from app.models import User
from app import app 
import requests

#################
# Custom Fields
#################

class TagListField(Field):
	widget = TextInput()

	def _value(self):
		if self.data:
			return u', '.join(self.data)
		else:
			return u''

	def process_formdata(self, valuelist):
		if valuelist:
			self.data = [x.strip() for x in valuelist[0].split(',')]
		else:
			self.data = []

class BetterTagListField(TagListField):
	def __init__(self, label='', validators=None, remove_duplicates=True, **kwargs):
		super(BetterTagListField, self).__init__(label, validators, **kwargs)
		self.remove_duplicates = remove_duplicates

	def process_formdata(self, valuelist):
		super(BetterTagListField, self).process_formdata(valuelist)
		if self.remove_duplicates:
			self.data = list(self._remove_duplicates(self.data))

	@classmethod
	def _remove_duplicates(cls, seq):
		"""Remove duplicates in a case insensitive, but case preserving manner"""
		d = {}
		for item in seq:
			if item.lower() not in d:
				d[item.lower()] = True
				yield item

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

#########
# Forms
#########

class LoginForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	remember_me = BooleanField('Remember Me')
	submit = SubmitField('Sign in')
	
class RegistrationForm(FlaskForm):
	username = StringField('Username', validators=[DataRequired()])
	email = StringField('Email', validators=[DataRequired(), Email()])
	password = PasswordField('Password', validators=[DataRequired()])
	password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
	submit = SubmitField('Register')

	def validate_username(self, username):
		user = User.query.filter_by(username=username.data).first()
		if user is not None:
			raise ValidationError('Username already taken. Try another')

	def validate_email(self, email):
		user = User.query.filter_by(email=email.data).first()
		if user is not None:
			raise ValidationError('Email already in use. Try another')

	#def validate_email(self, email): 

class NewPassForm(FlaskForm):
	curr_password = PasswordField('Current Password', validators=[DataRequired()])
	password = PasswordField('New Password', validators=[DataRequired()])
	password2 = PasswordField('Repeat New Password', validators=[DataRequired(), EqualTo('password')])
	submit = SubmitField('Reset')

class NewGroupForm(FlaskForm):
	groupname = StringField('Group Name', validators=[DataRequired()])
	memberlist = StringField('Group Members') #just members to group
	adminlist = StringField('Group Admins') #admins can add new members
	grouplist = StringField('Subgroups') #a group whose members are members of this group
	submit = SubmitField('Create Group')

	def validate_groupname(self, groupname):
		#verify groups doesn't exist
		groups = groupname.data.split(',')
		for group in groups:
			r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/groups/{group}", auth=app.config['ADMIN_AUTH'])
			if r.status_code == 200:
				raise ValidationError(f'Group {group} exists. Please try another name')

	def validate_memberlist(self, memberlist):
		if not memberlist.data:
			return
		#verify users exist
		members = memberlist.data.split(',')
		for member in members:
			u = User.query.filter_by(username=member).first()
			if u is None:
				raise ValidationError(f'User {member} does not exist. Double check group members')

	def validate_adminlist(self, adminlist):
		if not adminlist.data:
			return
		#verify users exist
		members = adminlist.data.split(',')
		for member in members:
			u = User.query.filter_by(username=member).first()
			if u is None:
				raise ValidationError(f'User {member} does not exist. Double check admin members')

	def validate_grouplist(self, grouplist):
		if not grouplist.data:
			return
		#verify groups exist
		groups = grouplist.data.split(',')
		for group in groups:
			r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/groups/{group}", auth=app.config['ADMIN_AUTH'])
			if r.status_code != 200:
				raise ValidationError(f'Group {group} does not exist. Double check subgroups')

class NewDatasetForm(FlaskForm):
	title = StringField('Title', validators=[DataRequired()])
	files = MultipleFileField('File(s)', validators=[DataRequired()]) #no filetype restriction currently
	tags = BetterTagListField('Categories')
	world_readable = BooleanField('Visible by all?')
	account_read_permissions = StringField('Users with read access')
	account_write_permissions = StringField('Users with write access')
	group_read_permissions = StringField('Groups with read access')
	group_write_permissions = StringField('Groups with write access')
	submit = SubmitField('Upload')

	def validate_account_read_permissions(self, memberlist):
		if not memberlist.data:
			return
		#verify users exist
		members = memberlist.data.split(',')
		for member in members:
			u = User.query.filter_by(username=member).first()
			if u is None:
				raise ValidationError(f'User {member} does not exist. Double check group members')

	def validate_account_write_permissions(self, memberlist):
		if not memberlist.data:
			return
		#verify users exist
		members = memberlist.data.split(',')
		for member in members:
			u = User.query.filter_by(username=member).first()
			if u is None:
				raise ValidationError(f'User {member} does not exist. Double check group members')

	def validate_group_read_permissions(self, grouplist):
		if not grouplist.data:
			return
		#verify groups exist
		groups = grouplist.data.split(',')
		for group in groups:
			r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/groups/{group}", auth=app.config['ADMIN_AUTH'])
			if r.status_code != 200:
				raise ValidationError(f'Group {group} does not exist. Double check subgroups')

	def validate_group_write_permissions(self, grouplist):
		if not grouplist.data:
			return
		#verify groups exist
		groups = grouplist.data.split(',')
		for group in groups:
			r = requests.get(f"{app.config['KINTO_ADDR']}/v1/buckets/datasets/groups/{group}", auth=app.config['ADMIN_AUTH'])
			if r.status_code != 200:
				raise ValidationError(f'Group {group} does not exist. Double check subgroups')

class DatasetSearchForm(FlaskForm):
	tags = BetterTagListField('Search by categories', validators=[DataRequired()])
	submit = SubmitField()

class DatasetFilterDownloadForm(FlaskForm):
	id_filter = StringField('Filter by name', validators=[DataRequired()]) #if more filters are created, this will not necessarily be required
	submitFilter = SubmitField('Download')

class DatasetSelectDownloadForm(FlaskForm):
	"""
	remember to add SelectField choices at form instanciation
	No validators on the multiple checkbox field so that only those requested
	by user are downloaded. Need to check if args are empty at download
	endpoint
	"""
	dataset_list = MultiCheckboxField('Select Datasets to Download')
	submitSelect = SubmitField('Download')