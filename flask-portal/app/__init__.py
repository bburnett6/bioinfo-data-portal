from flask import Flask
from flask_login import LoginManager
from flask_dance.contrib.google import make_google_blueprint, google
from flask_sqlalchemy import SQLAlchemy 
from flask_migrate import Migrate 
from config import Config 
import sys

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
google_blueprint = make_google_blueprint(scope=["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email"]) #Assumes proper oauth vars set in config
app.register_blueprint(google_blueprint, url_prefix="/login")
login = LoginManager(app)
login.login_view = 'google.login'

from app import routes, models

db.create_all()