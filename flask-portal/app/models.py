
import requests
from uuid import uuid4
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin, login_user, current_user
from flask import flash
from sqlalchemy.orm.exc import NoResultFound
from flask_dance.consumer.storage.sqla import OAuthConsumerMixin, SQLAlchemyStorage
from flask_dance.consumer import oauth_authorized, oauth_error
from flask_dance.contrib.google import google 
from app import app, login, db, google_blueprint
import sys

class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	username = db.Column(db.String(64), index=True, unique=True)
	email = db.Column(db.String(120), index=True, unique=True)
	fullname = db.Column(db.String(64))
	api_token = db.Column(db.String(128))

	def gen_api_token(self):
		#Might need to look into another method of creating a random api token.
		#This is used to authenticate with the api server so more thought 
		#might be needed.
		self.api_token = str(uuid4())

	def get_api_token(self):
		return str(self.api_token)

	def set_fullname(self, name):
		self.fullname = name

	def __repr__(self):
		return f'<User {self.username}>'

"""
Flask-Dance google oauth stuff
Based on the sample project here: 
https://github.com/singingwolfboy/flask-dance-google-sqla
That repo has all the steps and requirements for setting
google oauth up correctly
"""

class OAuth(OAuthConsumerMixin, db.Model):
	provider_user_id = db.Column(db.String(256), unique=True)
	user_id = db.Column(db.Integer, db.ForeignKey(User.id))
	user = db.relationship(User)

google_blueprint.storage = SQLAlchemyStorage(OAuth, db.session, user=current_user)

@login.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

@oauth_authorized.connect_via(google_blueprint)
def google_logged_in(blueprint, token):
	if not token:
		flash("Failed to login with Google...")
		return False 

	resp = google.get("/oauth2/v1/userinfo")
	#print("response from google:", file=sys.stderr)
	#print(f"{resp}", file=sys.stderr)
	if not resp.ok:
		flash("Failed to fetch user info from Google...")
		return False 

	google_info = resp.json()
	"""
	google_info shape example:
	{
		'id': '100358018683857850111', 
		'email': 'bburnett@umassd.edu', 
		'verified_email': True, 
		'name': 'Ben Burnett', 
		'given_name': 'Ben', 
		'family_name': 'Burnett',
	 	'picture': 'https://lh3.googleusercontent.com/a/AATXAJzt7yd9CmpPaflQ2JQ161vvdRoJ9DeF9gWkep6A=s96-c',
	 	'locale': 'en'
	 }
	"""
	#print(f"{google_info}", file=sys.stderr)
	google_uid = str(google_info['id'])

	query = OAuth.query.filter_by(provider=blueprint.name, provider_user_id=google_uid)
	try:
		oauth = query.one()
	except NoResultFound:
		oauth = OAuth(provider=blueprint.name, provider_user_id=google_uid, token=token)

	if oauth.user: #we recognize this user. Just login
		login_user(oauth.user)
		flash(f"Login with Google Successful! Welcome back {oauth.user.fullname}")
	else: #users first time logging in. Create new user
		user = User(username=google_uid, email=google_info['email'], fullname=google_info['name'])
		user.gen_api_token()
		oauth.user = user 
		db.session.add_all([user, oauth])
		db.session.commit()
		#create users api account 
		data = {"data": {"password": user.get_api_token()}}
		r = requests.put(f"{app.config['KINTO_ADDR']}/v1/accounts/{user.username}", json=data)
		if r.status_code != 201:
			print(f"Error initializing user, {user.username}, with api. Data: {data}. Status: {r.status_code}.", file=sys.stderr)
		#login user
		login_user(user)
		flash("Login with Google Successful! Looks like this is your first time, Welcome!")

	return False 

