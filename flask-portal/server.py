#!/usr/bin/env python
import os 
import sys
import time
import requests

from app import app 
from config import Config

if __name__ == "__main__":
	#verify or init admin account
	for i in range(10): #10 attempts to wait for kinto to start up
		try:
			#attempt to authorize admin
			r = requests.get(f'{Config.KINTO_ADDR}/v1/accounts/admin', auth=Config.ADMIN_AUTH)
			#if admin is not authorized create admin
			if r.status_code != 200: 
				data = {"data": {"password": Config.ADMIN_AUTH[1]}} #admin_auth = ('uname', 'passwd')
				r = requests.put(f'{Config.KINTO_ADDR}/v1/accounts/admin', json=data)
				if r.status_code != 201:
					print(f"Error initializing admin account. Response: {r.status_code}", file=sys.stderr)
				else:
					print(f"Kinto admin account initialized.", file=sys.stderr)
				#initialize datasets bucket with admin
				data = {'data': {'id': 'datasets'}, 'permissions': {}}
				r = requests.post(f"{Config.KINTO_ADDR}/v1/buckets", json=data, auth=Config.ADMIN_AUTH)
				if r.status_code != 201:
					print(f"Error initializing datasets bucket.", file=sys.stderr)
				else:
					print(f"Initialized datasets bucket.", file=sys.stderr)
			else:
				print("Kinto admin account verified.", file=sys.stderr)
				break
		except Exception as e:
			print(f"connection refused. Attempt {i}", file=sys.stderr)
			print(f"Exception: {e}", file=sys.stderr)
			time.sleep(1.0)

	#Run app
	app.run(host='0.0.0.0', port=os.environ.get("FLASK_PORTAL_PORT", 9090), debug=True)
